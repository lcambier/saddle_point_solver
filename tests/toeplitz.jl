function toeplitz(m,n)

	F = spdiagm((-1*ones(m-2),1*ones(m-1),5*ones(m),1*ones(m-1),-1*ones(m-2)),(-2,-1,0,1,2),m,m)
	G = spdiagm(( 1*ones(n-2),2*ones(n-1),7*ones(n),2*ones(n-1), 1*ones(n-2)),(-2,-1,0,1,2),n,n)
	if m < n
		H = spdiagm( ( -5*ones(m-1) , 3*ones(m) , 2*ones(m)   ) , (-1,0,1) , m,n)
	else
		H = spdiagm( ( -5*ones(n)   , 3*ones(n) , 2*ones(n-1) ) , (-1,0,1) , m,n)
	end

	return (F,G,H)

end