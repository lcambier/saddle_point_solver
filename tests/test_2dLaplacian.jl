using Gadfly

include("../src/saddle_point_solve.jl") 
include("laplacian2d.jl")

(nx,ny) = (1000,1000)
F = laplacian2d(nx,ny)
H = spzeros(nx*ny,0)
G = spzeros(0,0)
f = randn(nx*ny,1)
g = zeros(0,1) 

tic()
(x,y) = saddle_point_solve(F,G,H,f,g) 
toc()

println("Done.") 

# Compare to straightforward implementation

A = [F   H ;
     H' -G]
b = [f ; g]
x = A\b 

tic()
println("Straighforward implementation residual : ", norm(A*x-b)/norm(b))
toc()


